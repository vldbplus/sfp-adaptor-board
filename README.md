# SFP Adaptor Board

The SFP adaptor Board can be used on the VLDB+ with a SFP transceiver instead of a VTRx+.
By replacing the FEASTMP2.5 by a 3.3 you can plug and power this board to receive and send
lpGBT data in the VLDB+.
